﻿namespace WebReportsExtensions
{
	using System;
	using WebReports.Api.Common;

	public partial class CustomFunctions
	{
		// This function loads a counter, increments by the input value (default = 1), then displays its value.
		// The first instance of the counter will be initialized to the input value.
		// This code is fully functional and is intended to be used as-is.
		// Return Type: Decimal
		// Prevent In-Database Grouping When Included in Detail Section: True
		// Usage: Increment()                   -> increments a counter with the default key by 1, then displays
		//        Increment(byValue)            -> increments a counter with the default key by the input value, then displays
		//		  Increment(byValue, uniqueKey) -> increments a counter with a specified key by the input value, then displays
		//                                         used for reports with more than one distinct counter
		public static object Increment(SessionInfo sessionInfo, params object[] args)
		{
			double inc = 1;
			if (args.Length > 0 && !double.TryParse(args[0].ToString(), out inc))
				return "Error: Input value was not a number";

			string key = args.Length > 1 ? args[1].ToString() : "__default_counter";

			double counter = (int)sessionInfo.GetStoredValue(key, 0);

			sessionInfo.SetStoredValue(key, counter += inc);

			return counter;
		}
	}
}
