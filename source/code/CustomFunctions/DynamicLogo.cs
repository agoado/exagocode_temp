﻿namespace WebReportsExtensions
{
	using System;
	using WebReports.Api.Common;
	using WebReports.Api.Reports;

	public partial class CustomFunctions
	{
		// This function displays an image whose path is dependent on the value of a session parameter.
		// This code is example code that must be modified for your environment and is not intended to be used as-is.
		// Namespaces: WebReports.Api.Reports
		// Return Type: String
		// Usage: DynamicLogo()

		public static object DynamicLogo(SessionInfo sessionInfo, params object[] args)
		{
			// EXAMPLE CODE start

			string imagePath = sessionInfo.GetParameter("logoPath").Value;

			// If LoadImage Prefix is set, use a relative file path.
			// If LoadImage Prefix is not set, use an absolute file path.

			// EXAMPLE CODE end

			string formulaText = string.Format("=LoadImage(\"{0}\")", imagePath);
			CellFormula formula = CellFormula.CreateFormula(sessionInfo.PageInfo, formulaText, CellVariableCollectionFilter.DataField);
			return formula.Evaluate(null);
		}
	}
}
