﻿namespace WebReportsExtensions
{
	using System;
	using System.Drawing;
	using System.Drawing.Imaging;
	using WebReports.Api.Common;
	using WebReports.Api.Reports;
	using BarcodeLib;

	public partial class CustomFunctions
	{
		// This function creates a barcode of the specified type based on the numeric input value.
		// This code is fully functional and is intended to be used as-is.
		// Requires the third party library: https://github.com/barnhill/barcodelib
		// References: System.Drawing.dll;BarcodeLib.dll
		// Namespaces: System.Drawing;System.Drawing.Imaging;WebReports.Api.Reports;BarcodeLib
		// Return Type: String
		/*
		 * Usage
			Barcode(value, type)
			Barcode(value, type, cell)
			Barcode(value, type, width, height)
			•	value is the value to be encoded into the barcode
			•	type is the type of barcode (refer to the list of barcode types below)
			•	Optional: cell is the index of the current cell, in the format "A#", where A is the letter of the cell row
						  and # is the number of the cell column. This scales the barcode to the cell size.
			•	Optional: width and height are integer values for the width and height of the barcode.
			Barcode Types
			•	UNSPECIFIED
			•	UPCA
			•	UPCE
			•	UPC_SUPPLEMENTAL_2DIGIT
			•	UPC_SUPPLEMENTAL_5DIGIT
			•	EAN13
			•	EAN8
			•	Interleaved2of5
			•	Standard2of5
			•	Industrial2of5
			•	CODE39
			•	CODE39Extended
			•	CODE39_Mod43
			•	Codabar
			•	PostNet
			•	BOOKLAND
			•	ISBN
			•	JAN13
			•	MSI_Mod10
			•	MSI_2Mod10
			•	MSI_Mod11
			•	MSI_Mod11_Mod10
			•	Modified_Plessey
			•	CODE11
			•	USD8
			•	UCC12
			•	UCC13
			•	LOGMARS
			•	CODE128
			•	CODE128A
			•	CODE128B
			•	CODE128C
			•	ITF14
			•	CODE93
			•	TELEPEN
			•	FIM
			•	PHARMACODE
			*/

		public static object Barcode(SessionInfo sessionInfo, params object[] args)
		{
			string value = args[0].ToString();

			TYPE type;
			if (!Enum.TryParse(args[1].ToString(), true, out type))
				return string.Format("Error: Invalid barcode type '{0}'", args[1]);

			int width = 200;
			int height = 100;

			if (args.Length == 4)
			{
				if (!int.TryParse(args[2].ToString(), out width))
					return string.Format("Error: Width '{0}' must be an integer", args[2]);
				if (!int.TryParse(args[3].ToString(), out height))
					return string.Format("Error: Height '{0}' must be an integer", args[3]);
			}

			if (args.Length == 3)
			{
				string cell = args[2].ToString().ToLower();
				if (cell.Length < 2)
					return string.Format("Error: '{0}' must be in the form 'A1'", args[2]);

				int colIdx = cell[0] - 97;
				if (colIdx < 0 || colIdx > 25)
					return string.Format("Error: '{0}' must be in the form 'A1'", args[2]);

				int rowIdx;
				if (!int.TryParse(cell.Substring(1), out rowIdx))
					return string.Format("Error: '{0}' must be in the form 'A1'", args[2]);
				rowIdx--;

				if (sessionInfo.Report.Cells.GetCell(rowIdx, colIdx) == null)
					return string.Format("Error: Cell '{0}' does not exist", args[2]);

				width = sessionInfo.Report.Columns[colIdx].Width;
				height = sessionInfo.Report.Rows[rowIdx].Height;
			}

			using (Barcode barcode = new Barcode()
			{
				Width = width,
				Height = height,
				ForeColor = Color.Black,
				BackColor = Color.White,
				Alignment = AlignmentPositions.CENTER,
				IncludeLabel = true,
				ImageFormat = ImageFormat.Png
			})
			{
				barcode.Encode(type, value);

				return new UdfLoadImageData()
				{
					ImageTag = Base64Encoding.Encode(barcode.GetImageData(SaveTypes.PNG)),
					ImageType = UdfLoadImageType.Db
				};
			}
		}
	}
}
