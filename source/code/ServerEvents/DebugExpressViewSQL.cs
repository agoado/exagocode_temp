﻿namespace WebReportsExtensions
{
	using WebReports.Api.Common;

	public partial class ServerEvents
	{
		// OnExecuteSQLStatementConstructed
		// This server event is triggered once the report SQL statement is constructed during the execution process.
		// This server event shows the constructed report SQL statement when the Run button is clicked. It is useful
		// for report types or older versions of Exago BI (pre-v2019.1) that do not have the Show SQL button available.
		// This code is fully functional and is intended to be used as-is.
		// This event is intended to be active only temporarily, and in a non-production testing environment.
		public static void DebugExpressViewSQL(SessionInfo sessionInfo, params object[] args)
		{
			throw new WrUserMessage(args[0].ToString(), WrUserMessageType.Text);
		}
	}
}
