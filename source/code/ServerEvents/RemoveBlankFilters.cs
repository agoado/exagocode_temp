﻿namespace WebReportsExtensions
{
	using WebReports.Api.Reports;
	using WebReports.Api.Common;

	public partial class ServerEvents
	{
		// OnReportExecuteStart
		// This server event is triggered at the beginning of the report execution process.
		// This server event dynamically removes blank filters from reports when they execute.
		// This code is fully functional and is intended to be used as-is.
		// Namespaces: WebReports.Api.Reports
		public static string RemoveBlankFilters(SessionInfo sessionInfo)
		{
			ReportFilterCollection filters = sessionInfo.Report.ExecFilters;

			for (int i = 0; i < filters.Count; i++)
			{
				Filter filter = filters[i];

				if (filter.Value.Length == 0)
				{
					filters.RemoveAt(i);
					i--;
				}
			}

			return null;
		}
	}
}
